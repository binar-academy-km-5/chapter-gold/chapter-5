package com.mertechin.challenge5.ui.register

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.mertechin.challenge5.MainActivity
import com.mertechin.challenge5.databinding.ActivityRegisterBinding

class RegisterActivity : AppCompatActivity() {
	private lateinit var vmRegister: RegisterViewModel
	private lateinit var binding : ActivityRegisterBinding

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		binding = ActivityRegisterBinding.inflate(layoutInflater)
		setContentView(binding.root)

		vmRegister = ViewModelProvider(this).get(RegisterViewModel::class.java)
		vmRegister.initSharedPref(this)

		// Ambil nilai dari form Login
		val user    = binding.etUsername.text
		val email   = binding.etEmail.text
		val pass    = binding.etPassword.text
		val notelp  = binding.etNoTelp.text

		binding.btnRegister.setOnClickListener {
			// Firebase Register Algorithm
			vmRegister.registerFirebase(email.toString(),pass.toString(),object : RegisterViewModel.RegisterCallback{
				override fun onRegisterSuccess() {
					// Munculin Pop-up
					val toast = Toast.makeText(this@RegisterActivity,"Registrasi berhasil! Selamat datang, "+user.toString()+"!", Toast.LENGTH_SHORT)
					toast.show()

					// pindah ke activity selanjutnya
					val intent = Intent(this@RegisterActivity,MainActivity::class.java)
					startActivity(intent)
				}

				override fun onRegisterFailure() {
					val toast = Toast.makeText(this@RegisterActivity,"Registrasi gagal!", Toast.LENGTH_SHORT)
					toast.show()
				}

			})

			// Move to Next Page..
			val i = Intent(this,MainActivity::class.java)
			startActivity(i)
		}
	}
}