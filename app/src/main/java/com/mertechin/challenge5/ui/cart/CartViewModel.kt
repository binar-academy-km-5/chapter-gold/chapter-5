package com.mertechin.challenge5.ui.cart

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.mertechin.challenge5.database.RestoranDatabase
import com.mertechin.challenge5.dataclass.Restoran

class CartViewModel: ViewModel() {
	fun getCart(context: Context): LiveData<List<Restoran>>{
		val storage = RestoranDatabase.getInstance(context).restoranDao
		return storage.getAllCart()
	}

	fun getSUM(context: Context): Int {
		val storage = RestoranDatabase.getInstance(context).restoranDao
		val totalHarga: Int = storage.measure()
		return totalHarga
	}
}