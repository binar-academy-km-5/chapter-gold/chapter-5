package com.mertechin.challenge5.ui.login

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.util.Log
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

class LoginViewModel : ViewModel() {
	private lateinit var auth: FirebaseAuth
	private lateinit var sharedPreferences: SharedPreferences

	fun initSharedPref(context: Context){
		sharedPreferences = context.getSharedPreferences("login_pref", Context.MODE_PRIVATE)
	}

	fun loginFirebase(email: String, password: String, callback: LoginCallback) {
		auth = Firebase.auth

		auth.signInWithEmailAndPassword(email, password)
			.addOnCompleteListener {
				if (it.isSuccessful) {
					// Daftarkan sharedPref
					setSharedPref(email)

					// Memberi callback Success
					callback.onLoginSuccess()
				} else {
					// User belum terdaftar, Maka berikan callback Gagal
					callback.onLoginFailure()
				}
			}
	}

	fun setSharedPref(email: String){
		val editor = sharedPreferences.edit()
		editor.putString("USER_EMAIL",email)
		editor.apply()
	}

	interface LoginCallback {
		fun onLoginSuccess()
		fun onLoginFailure()
	}

}