package com.mertechin.challenge5.ui.detail

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.mertechin.challenge5.MainActivity
import com.mertechin.challenge5.database.RestoranDatabase
import com.mertechin.challenge5.databinding.ActivityDetailBinding
import com.mertechin.challenge5.dataclass.Restoran
import com.mertechin.challenge5.dataclass.data_menu

class DetailActivity : AppCompatActivity() {
	private lateinit var binding : ActivityDetailBinding
	private lateinit var viewModel: DetailViewModel

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)

		viewModel   = ViewModelProvider(this).get(DetailViewModel::class.java)
		binding     = ActivityDetailBinding.inflate(layoutInflater)
		setContentView(binding.root)

		//inisialisasi Database
		val storage = RestoranDatabase.getInstance(this).restoranDao

		// Mengambil data dari halaman sebelumnya
		val data        = intent.getParcelableExtra<data_menu>("DATA")

		// Memasukkan data pada viewModel
		if (data != null) {
			viewModel.setData(data)
		}

		// Observer untuk LiveData
		viewModel.data.observe(this, Observer { data ->
			binding.imgDetail.setImageResource(data.image)
			binding.nameDetail.text     = data.title
			binding.priceDetail.text    = data.price.toString()
			binding.descDetail.text     = data.desc
		})

		binding.btnDetail.setOnClickListener{
			// Masukkan value ke dalam database
			data?.let {
				val restoranData = Restoran(
					name = it.title.toString(),
					price = it.price, // Ubah tipe data sesuai dengan definisi di data class
					quantity = 1, // Ganti dengan kuantitas yang sesuai
					note = "",
					img = it.image
				)

				// Memasukkan data ke dalam database
				storage.insert(restoranData)
				Toast.makeText(this, "Pesanan sudah di keranjang!", Toast.LENGTH_SHORT).show()
//				Log.e("Database Check", storage.getAllCart().toString())
			}

			// Lanjut ke Halaman Berikutnya
			val intent = Intent(this,MainActivity::class.java)
			startActivity(intent)
		}
	}
}