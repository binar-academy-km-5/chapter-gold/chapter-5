package com.mertechin.challenge5.ui.splashScreen

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.ViewModelProvider
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.mertechin.challenge5.MainActivity
import com.mertechin.challenge5.databinding.ActivitySplashScreenBinding
import com.mertechin.challenge5.ui.login.LoginActivity

class SplashScreenActivity : AppCompatActivity() {
	private lateinit var binding: ActivitySplashScreenBinding
	private lateinit var splashViewModel: SplashViewModel

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		binding = ActivitySplashScreenBinding.inflate(layoutInflater)
		setContentView(binding.root)

		// Inisialisasi Crashlytics
		FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(true)

		// inisialisasi sharedPreferences
		splashViewModel = ViewModelProvider(this).get(SplashViewModel::class.java)
		splashViewModel.initSharedPref(this)

		// Thread untuk menunda tampilan utama selama 2 detik
		val background = object : Thread() {
			override fun run() {
				try {
					// Menunggu 2 detik
					Thread.sleep(2000)

					val LoggedIn        = splashViewModel.checkPref()
					Log.e("SharedBool", LoggedIn.toString())
					val targetActivity  =
						if (LoggedIn) {
							Log.e("SharedPref Detected!", splashViewModel.getPref())
							MainActivity::class.java
						} else {
							Log.e("SharedPref Undetected!", splashViewModel.getPref())
							LoginActivity::class.java
						}

					// Setelah 2 detik, pindah ke Activity selanjutnya
					val intent = Intent(applicationContext, targetActivity)
					startActivity(intent)
				} catch (e: Exception) {
					e.printStackTrace()
				}
			}
		}
		background.start()
	}
}