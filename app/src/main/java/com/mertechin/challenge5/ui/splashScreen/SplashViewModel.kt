package com.mertechin.challenge5.ui.splashScreen

import android.content.Context
import android.content.SharedPreferences
import androidx.lifecycle.ViewModel

class SplashViewModel : ViewModel() {
	private lateinit var sharedPreferences: SharedPreferences

	fun initSharedPref(context: Context){
		sharedPreferences = context.getSharedPreferences("login_pref", Context.MODE_PRIVATE)
	}

	fun getPref() : String{
		return sharedPreferences.getString("USER_EMAIL", null).toString()
	}

	fun checkPref(): Boolean {
		val userEmail = sharedPreferences.getString("USER_EMAIL", null).toString()

		return userEmail.isNullOrEmpty()
	}
}