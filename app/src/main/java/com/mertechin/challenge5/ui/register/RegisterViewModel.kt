package com.mertechin.challenge5.ui.register

import android.content.Context
import android.content.SharedPreferences
import androidx.lifecycle.ViewModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

class RegisterViewModel : ViewModel() {
	private lateinit var auth: FirebaseAuth
	private lateinit var sharedPreferences: SharedPreferences

	fun initSharedPref(context: Context){
		sharedPreferences = context.getSharedPreferences("login_pref", Context.MODE_PRIVATE)
	}

	fun registerFirebase(email: String, password: String, callback: RegisterCallback) {
		auth = Firebase.auth

		auth.createUserWithEmailAndPassword(email, password)
			.addOnCompleteListener {
				if (it.isSuccessful) {
					// Daftarkan sharedPref
					setSharedPref(email)

					// Memberi callback Success
					callback.onRegisterSuccess()
				} else {
					// User belum terdaftar, Maka berikan callback Gagal
					callback.onRegisterFailure()
				}
			}
	}

	fun setSharedPref(email: String){
		val editor = sharedPreferences.edit()
		editor.putString("USER_EMAIL",email)
		editor.apply()
	}

	interface RegisterCallback {
		fun onRegisterSuccess()
		fun onRegisterFailure()
	}
}