package com.mertechin.challenge5.ui.login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.mertechin.challenge5.MainActivity
import com.mertechin.challenge5.databinding.ActivityLoginBinding
import com.mertechin.challenge5.ui.register.RegisterActivity

class LoginActivity : AppCompatActivity() {
	private lateinit var binding : ActivityLoginBinding
	private lateinit var loginViewModel: LoginViewModel

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		binding = ActivityLoginBinding.inflate(layoutInflater)
		setContentView(binding.root)

		loginViewModel = ViewModelProvider(this).get(LoginViewModel::class.java)

		//Inisialisasi sharedPref
		loginViewModel.initSharedPref(this)

		// Ambil nilai dari form Login
		val email = binding.etEmail.text
		val pass  = binding.etPassword.text

		binding.btnLogin.setOnClickListener{
			// Verifikasi ke Firebase

			Log.e("LOGIN", email.toString())
			Log.e("LOGIN", pass.toString())

			loginViewModel.loginFirebase(email.toString(),pass.toString(),object : LoginViewModel.LoginCallback{
				override fun onLoginSuccess() {
					// pindah ke activity selanjutnya
					val intent = Intent(this@LoginActivity,MainActivity::class.java)
					startActivity(intent)
				}

				override fun onLoginFailure() {
					val toast = Toast.makeText(this@LoginActivity,"Email atau Password Salah!", Toast.LENGTH_SHORT)
					toast.show()
				}
			})
		}

		binding.tvRegister.setOnClickListener {
			val intent = Intent(this,RegisterActivity::class.java)
			startActivity(intent)
		}
	}
}