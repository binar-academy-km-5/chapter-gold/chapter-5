package com.mertechin.challenge5.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.textfield.TextInputEditText
import com.mertechin.challenge5.R
import com.mertechin.challenge5.dataclass.Restoran

class CartAdapter(private val data: List<Restoran>) : RecyclerView.Adapter<MenuHolder>() {
	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuHolder {
		val inflater = LayoutInflater.from(parent.context)
		val view = inflater.inflate(R.layout.cart_menu_item, parent, false)
		return MenuHolder(view)
	}

	override fun onBindViewHolder(holder: MenuHolder, position: Int) {
		val menuHolder = holder
		menuHolder.onBind(data[position])
	}

	override fun getItemCount(): Int = data.size
}

class MenuHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
	val ivGambar: ImageView = itemView.findViewById(R.id.imageView2)
	val tvNama: TextView = itemView.findViewById(R.id.textView6)
	val tvHarga: TextView = itemView.findViewById(R.id.textView11)
	val ivDelete: ImageView = itemView.findViewById(R.id.ivDelete)
	val ivMinus: ImageView = itemView.findViewById(R.id.ivMinus)
	val ivPlus: ImageView = itemView.findViewById(R.id.ivPlus)
	val etQuantity: EditText = itemView.findViewById(R.id.etJumlah)
	val tilNote: TextInputEditText = itemView.findViewById(R.id.note)

	fun onBind(dataCart: Restoran) {
		// Isi elemen UI dengan data dari data_cart
		ivGambar.setImageResource(dataCart.img)
		tvNama.text     = dataCart.name
		tvHarga.text    = dataCart.price.toString()

		etQuantity.setText(dataCart.quantity.toString())
		tilNote.setText(dataCart.note)
	}
}
