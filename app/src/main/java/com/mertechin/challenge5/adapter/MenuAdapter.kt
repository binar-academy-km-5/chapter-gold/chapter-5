package com.mertechin.challenge5.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.mertechin.challenge5.R
import com.mertechin.challenge5.dataclass.data_menu

class MenuAdapter(private val isGrid: Boolean, private val data: List<data_menu>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

	private var onItemClickCallback: OnItemClickCallback? = null

	fun setOnItemClickCallback(callback: OnItemClickCallback) {
		onItemClickCallback = callback
	}

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
		val inflater = LayoutInflater.from(parent.context)
		val layoutRes = if (isGrid) R.layout.grid_item_menu else R.layout.linear_item_menu
		val view = inflater.inflate(layoutRes, parent, false)

		return if (isGrid) GridMenuHolder(view) else LinearMenuHolder(view)
	}

	override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
		val menuHolder = holder as MenuHolder
		menuHolder.onBind(data[position])

		menuHolder.itemView.setOnClickListener {
			onItemClickCallback?.onItemClicked(data[position])
		}
	}

	override fun getItemCount(): Int = data.size

	interface OnItemClickCallback {
		fun onItemClicked(data: data_menu)
	}

	abstract class MenuHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
		abstract fun onBind(menu: data_menu)
	}

	inner class GridMenuHolder(itemView: View) : MenuHolder(itemView) {
		private val gambar: ImageView = itemView.findViewById(R.id.image_menu_grid)
		private val tvJudul: TextView = itemView.findViewById(R.id.title_menu_grid)
		private val tvHarga: TextView = itemView.findViewById(R.id.price_menu_grid)

		override fun onBind(menu: data_menu) {
			val (image, title, price) = menu
			tvJudul.text = title
			tvHarga.text = price.toString()
			gambar.setImageResource(image)
		}
	}

	inner class LinearMenuHolder(itemView: View) : MenuHolder(itemView) {
		private val gambar: ImageView = itemView.findViewById(R.id.image_menu_linear)
		private val tvJudul: TextView = itemView.findViewById(R.id.title_menu_linear)
		private val tvHarga: TextView = itemView.findViewById(R.id.price_menu_linear)

		override fun onBind(menu: data_menu) {
			val (image, title, price) = menu
			tvJudul.text = title
			tvHarga.text = price.toString()
			gambar.setImageResource(image)
		}
	}
}


