package com.mertechin.cobaapi.Model.KevinAPI

data class DataX(
    val createdAt: String,
    val id: Int,
    val imageUrl: String,
    val nama: String,
    val updatedAt: String
)