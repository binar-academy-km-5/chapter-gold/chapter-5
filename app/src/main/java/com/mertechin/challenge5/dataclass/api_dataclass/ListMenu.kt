package com.mertechin.cobaapi.Model.KevinAPI

data class ListMenu(
    val `data`: List<Data>,
    val message: String,
    val status: Boolean
)