package com.mertechin.cobaapi.Model.KevinAPI

data class CategoryMenuResponse(
    val `data`: List<DataX>,
    val message: String,
    val status: Boolean
)