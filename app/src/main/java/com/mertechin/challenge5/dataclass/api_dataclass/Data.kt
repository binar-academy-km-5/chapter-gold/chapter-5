package com.mertechin.cobaapi.Model.KevinAPI

data class Data(
    val alamatResto: String,
    val createdAt: String,
    val detail: String,
    val harga: Int,
    val hargaFormat: String,
    val id: Int,
    val imageUrl: String,
    val nama: String,
    val updatedAt: String
)