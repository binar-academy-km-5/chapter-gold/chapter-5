package com.mertechin.challenge5.dataclass

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tbl_keranjang")

data class Restoran(
	@PrimaryKey(autoGenerate = true) val id: Long? = null,
	@ColumnInfo(name = "name") val name: String,
	@ColumnInfo(name = "price") val price: Int,
	@ColumnInfo(name = "quantity") val quantity: Int,
	@ColumnInfo(name = "note") val note:String,
	@ColumnInfo(name = "img") val img : Int
)

