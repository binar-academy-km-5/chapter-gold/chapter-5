package com.mertechin.challenge5.api

import com.mertechin.cobaapi.Model.KevinAPI.CategoryMenuResponse
import com.mertechin.cobaapi.Model.KevinAPI.ListMenu
import retrofit2.Call
import retrofit2.http.GET

interface ApiServices {
	@GET("listmenu")
	fun getListMenu() : Call<ListMenu>

	@GET("category-menu")
	fun getCategory() : Call<CategoryMenuResponse>
}