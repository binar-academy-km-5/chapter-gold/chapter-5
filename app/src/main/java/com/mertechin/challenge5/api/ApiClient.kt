package com.mertechin.challenge5.api

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

interface ApiClient {
	object ApiClient {
		const val BASE_URL = "https://testing.jasa-nikah-siri-amanah-profesional.com"

		private val logging : HttpLoggingInterceptor
			get() {
				val httpLoggingInterceptor = HttpLoggingInterceptor()
				return httpLoggingInterceptor.apply {
					httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
				}
			}
		private val client = OkHttpClient.Builder()
			.addInterceptor(logging)
			.build()
		val instance: ApiServices by lazy {
			val retrofit = Retrofit.Builder()
				.baseUrl(BASE_URL)
				.addConverterFactory(GsonConverterFactory.create())
				.client(client)
				.build()

			retrofit.create(ApiServices::class.java)
		}
	}
}